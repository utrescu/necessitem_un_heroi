Resultat
--------------------------------------------------

Després de la batalla els batallons han quedat reduits de 10.000 homes a:

    .... Sobreviuen 81 soldats de 50000: 

    Batalló 1 : queden 1 soldats
    Batalló 2 : queden 73 soldats
    Batalló 3 : queden 4 soldats
    Batalló 5 : queden 3 soldats

Estadístiques per batalló
---------------------
Els resultats de cada batalló són (victòries/batalla):

    Batallo:1 Victories:4/11
    Batallo:2 Victories:5/7
    Batallo:3 Victories:3/8
    Batallo:4 Victories:7/12
    Batallo:5 Victories:6/10

Herois: 

	+--------------------------------------+-------------------------+------------+
	| id_soldat                            | nom                     | id_batallo |
	+--------------------------------------+-------------------------+------------+
	| 3967e02c-96e2-4c9d-abce-a4d990bc0a73 | Alfons Páramo Palomera  |          5 |
	| a4e96cc3-dfb7-4da6-8bbe-c61d0b8cbd41 | Homer Llesera Armengol  |          5 |
	| f77f8841-a9ce-4239-9426-ba2c1e2086b3 | Pitu Casajuana Olivella |          5 |
	+--------------------------------------+-------------------------+------------+    
