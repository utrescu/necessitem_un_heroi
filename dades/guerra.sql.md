

Resultat
--------------------------------------------------

Després de la batalla els batallons han quedat reduits de 10.000 homes a:

    Batalló 1:  4 soldats
    Batalló 3:  1 soldat
    Batalló 4: 34 soldats
    Batalló 5: 23 soldats
    .... Sobreviuen 62 soldats de 50000: 

Victòries per Batalló
--------------------------------------------------

Cada batalló ha guanyat de les batalles en que ha participat:

    Batallo:1 Victories:5/9
    Batallo:2 Victories:6/11
    Batallo:3 Victories:8/14
    Batallo:4 Victories:6/9
    Batallo:5 Victories:4/7

L'heroi (el que ha guanyat més batalles i està viu) és: 

    +--------------------------------------+------------------+------------+
    | id_soldat                            | nom              | id_batallo |
    +--------------------------------------+------------------+------------+
    | aab367f5-ace4-4747-9424-6dff8b33de59 | Alfons Hom Paret |          3 |
    +--------------------------------------+------------------+------------+

