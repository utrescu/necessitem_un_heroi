Resultat
--------------------------------------------------

Després de la batalla els batallons han quedat reduits de 10.000 homes a:

    .... Sobreviuen 24 soldats de 50000: 

    Batalló 2 : queden 16 soldats
    Batalló 3 : queden 1 soldats
    Batalló 4 : queden 7 soldats

Estadístiques per batalló
---------------------
Els resultats de cada batalló són (victòries/batalla):

    Batallo:1 Victories:2/7
    Batallo:2 Victories:5/10
    Batallo:3 Victories:6/12
    Batallo:4 Victories:5/7
    Batallo:5 Victories:0/4
    
Heroi:

	+--------------------------------------+--------------------+------------+
	| id_soldat                            | nom                | id_batallo |
	+--------------------------------------+--------------------+------------+
	| b4fe69da-750d-4faf-a4e0-c3a21a6561a7 | Joan Marín Lozano  |          3 |
	+--------------------------------------+--------------------+------------+
