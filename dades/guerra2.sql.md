

Resultat
--------------------------------------------------

Després de la batalla els batallons han quedat reduits de 10.000 homes a:

    Batalló 1: 4 soldats
    Batalló 2: 1 soldats
    Batalló 3: 1 soldats
    Batalló 5: 1 soldats

    .... Sobreviuen 7 soldats de 50000: 
    Baldiri Pallarol Mascarell(Soldat) Batalló 1
    Manel Pedrol Majós(Soldat) Batalló 1
    Gerard Puig Gasull(Soldat) Batalló 1
    Rufo Pascal Magrinyà(Soldat) Batalló 1
    Cesc Masdefiol Iranzo(Soldat) Batalló 2
    Joan Pascal Casajuana(Soldat) Batalló 3
    Didac Montió Picanyol(Soldat) Batalló 5

Estadístiques per batalló
---------------------
Resultats dels batallons (victòries/batalles): 

    Batallo:1 Victories:3/9
    Batallo:2 Victories:5/10
    Batallo:3 Victories:7/13
    Batallo:4 Victories:4/10
    Batallo:5 Victories:3/8

Per tant l'heroi és: 

    Joan Pascal Casajuana


