# Necessitem un Heroi!

Des del principi en La guerra entre el **regne de Cortària** i **la república de Computònia** hi ha hagut unes batalles molt cruentes en que s’han produït moltes baixes. Els soldats dels exèrcits s’han reduït d’una manera alarmant i tots dos bàndols creuen que la propera batalla pot ser la que determini el guanyador de la guerra.

![soldats](imatges/guerra0.png)

El problema és que tot i que el govern obliga els habitants de Cortària a apuntar-se a l’exèrcit, i per tant no hi hauran gaires problemes per trobar soldats, els nous soldats no tenen la moral gaire alta.

![soldats sense moral](imatges/guerra2.png)

El gran General Pacvinau, cap dels exèrcits de Cortària, creu que el que cal és fer una campanya propagandística per apujar la moral dels nous soldats. Per això cal trobar algun heroi: o sigui algú que hagi participat en tantes batalles victorioses de Cortària com sigui possible..

![General Pacvinau](imatges/guerra1.png)

Han entrat les dades de les batalles de la guerra que s’han produït fins ara en una base de dades amb aquesta estructura.

![Base de dades](imatges/guerradb.png)

El general considera una batalla victoriosa cada vegada que en un enfrontament amb l'exèrcit de Computònia es maten més soldats enemics que no pas els enemics en maten de seus.

## Tasca

Confiaven en que a través d’aquestes dades podrien trobar a quin soldat es pot fer passar per un heroi, però malauradament l’únic informàtic que quedava en tot el regne va morir en la darrera escaramussa…

1.  La reina de Cortària us ha contractat perquè a partir de les dades que hi ha en la base de dades que us proporcionen els hi digueu quin ha de ser el soldat que ha de ser promocionat a heroi del regne de Cortària

Les condicions són:

    -  Ha d’estar viu (li volen fer una gira per donar moral als reclutes)
    - Dels vius ha de ser el que hagi participat en més batalles guanyades (hi ha hagut més baixes
      de l’enemic que nostres)
    - Si hi ha més d'un soldat en les mateixes condicions, no importa, els convertiran a tots en herois
    - Només cobrareu si el resultat és correcte (no sigui que la premsa descobreixi que l’heroi és
      un defraudador d’hisenda, o un lladre de bancs, ...)

Es poden descarregar backups de les bases de dades MySQL des d'aquí:

* [Exemple1](dades/guerra.sql) : I, per petició popular, també dades sobre el resultat [Resultat](dades/guerra.sql.md)
* [Exemple2](dades/guerra2.sql) : I, per petició popular, també dades sobre el resultat [Resultat](dades/guerra2.sql.md)
* [Exemple3](dades/guerra3.sql) : I, per petició popular, també dades sobre el resultat [Resultat](dades/guerra3.sql.md)
* [Exemple4](dades/guerra4.sql) : I, per petició popular, també dades sobre el resultat [Resultat](dades/guerra4.sql.md)
